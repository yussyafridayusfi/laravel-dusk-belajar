@extends('layout')

@section('title')
Create
@endsection

@section('content')
    <a href="{{route('logout')}}" class="btn btn-primary" title="logout">LOGOUT</a>
    <br><br><br>
    <h3>DATA</h3>
	<form action="{{route ('user.store')}}" method="post">
		@csrf
	  <div class="form-group">
	    <label for="exampleInputEmail1">Name</label>
	    <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" placeholder="Enter Name">
	    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
	  </div>
	  <div class="form-group">
	    <label for="exampleInputPassword1">Password</label>
	    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
	  </div>
	  <button type="submit" class="btn btn-primary">Submit</button>
	</form>
	<hr>

	<table class="table table-bordered">
		<tr>
			<th>No</th>
			<th>Name</th>
			<th width="280px">Action</th>
		</tr>
		<?php
			$no=1;
		?>
		@foreach($users as $user)
		<tr>
			<td>{{ $no++ }}</td>
			<td>{{ $user->name }}</td>
			<td>
				<form action="{{ route('user.destroy',$user->id) }}" method="post">
					<a href="{{ route('user.edit',$user->id) }}" class="btn btn-info">edit</a>
					@csrf
					@method('delete')
					<button type="submit" class="btn btn-danger">delete</button>
				</form>
			</td>
		</tr>
		@endforeach
	</table>
@endsection