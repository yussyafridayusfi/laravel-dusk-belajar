@extends('layout')

@section('title')
Edit
@endsection

@section('content')
	<a href="{{route('logout')}}" class="btn btn-primary" title="logout">LOGOUT</a>

	<form action="{{ route('user.update',$users->id) }}" method="post">
		@csrf
		{{method_field('PUT')}}
		  <div class="form-group">
		    <label for="exampleInputEmail1">Name</label>
		    <input type="text" class="form-control" id="name" name="name" value="{{$users->name}}" aria-describedby="emailHelp" placeholder="Enter Name">
		    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Password</label>
		    <input type="password" class="form-control" id="password" value="{{$users->name}}" name="password" placeholder="Password">
		  </div>
		  <button type="submit" class="btn btn-primary">Update</button>
	</form>
@endsection