@extends('layout')

@section('title')
Login
@endsection

@section('content')
<form action="{{route('login')}}" method="post">
  @csrf
  <div class="form-group">
    <h4><center>LOGIN-PAGE</center></h4>
    <label for="Name">Name</label>
    <input type="text" class="form-control" id="name"  name="name" aria-describedby="emailHelp" placeholder="Enter Name">
    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Login</button>
</form>
@endsection