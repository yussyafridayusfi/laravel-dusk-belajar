<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\User;

class LoginTesting extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('LOGIN-PAGE!');
        });
    }

    // public function testLoginUser()
    // {
        // User::where('name','yuss')->delete();
        // User::create(['name' => 'yuss','password' => bcrypt('yuss')]    );
        // $user = User::where('name','yuss');
        // $this->browse(function (Browser $browser) {
       
          // $browser->visit('/')
                  // ->loginAs(User::where('name', 'yuss')->firstOrFail)
               // ->assertSee('LOGINPAGE')
               // ->type('name','yuss')
                  // ->type('password','yuss')
                  // ->press('Login')
                  // ->assertSee('DATA');
        // });
    // }
}
