<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Auth;
use Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('create',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name'=> $request->name,
            'password'=>bcrypt($request->password)
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::find($id);
        return view('edit',compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::where('id', $id)->update([
            'name'=> $request->name,
            'password'=>bcrypt($request->password)
        ]);
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::findOrFail($id);
        $users->delete();

        return redirect()->route('user.index');
    }

    public function getLogin() 
    {
        return view('index');
    }

    public function postLogin(Request $request)
    {
        // dd($request->all());
        Log::info('name '.$request->name.' password '.$request->password);
        $user = User::where('name' , $request->name)->first();
        // dd($user);
        if(!$user)
        {
            Log::info('Wrong Name');
            return redirect()->back();
        }

        if(!Hash::check($request->password, $user->password))
        {
            Log::info('Wrong Password');
            return redirect()->back();
        }
        
        Auth::attempt($request->only('name', 'password'));
        $user = auth()->user();

        return redirect()->route('user.index');
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
