<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','App\Http\Controllers\UserController@getLogin')->name('login');
Route::post('/','App\Http\Controllers\UserController@postLogin')->name('login');
Route::get('/logout','App\Http\Controllers\UserController@getLogout')->name('logout');


Route::group(['prefix' => '/user', 'as' => 'user.'], function () {
	Route::get('/data','App\Http\Controllers\UserController@index')->name('index')->middleware('auth');
	Route::post('/store','App\Http\Controllers\UserController@store')->name('store')->middleware('auth');
	Route::get('/{id}/edit','App\Http\Controllers\UserController@edit')->name('edit')->middleware('auth');
	Route::get('/{id}/show','App\Http\Controllers\UserController@show')->name('show')->middleware('auth');
	Route::put('/{id}/update','App\Http\Controllers\UserController@update')->name('update')->middleware('auth');
	Route::delete('/{id}/destroy','App\Http\Controllers\UserController@destroy')->name('destroy')->middleware('auth');
});
// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
